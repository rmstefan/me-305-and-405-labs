# -*- coding: utf-8 -*-
"""
Created on Thu Nov 19 22:45:50 2020

@author:  Becca Stefanich
"""
# this file will run on the nucleo 

import numpy as np
from pyb import UART
import matplotlib.pyplot as plt

from Lab4frontEnd import User_Interface, ReadEncoderData

myuart = UART(2)
while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')
        
# if the user presses G, begin collecting data
    # begins sampling from encoder at 5Hz for 10s unless S is pressed
# if the user presses S, stops collecting data
# generate an image (labeled axis) using matplotlib and a .CSV file of data (time stamps and encoder readings)
# data should be stored in nucleo for batch transmission back to laptop- array() module may be useful
# include FSM images


# plotting results
valuestoplot = np.array([])
plt.plot(valuestoplot)

# add labels and units to x- and y- axis
plt.xlabel('Time [s]')
Text(0.5, 0, 'Time [s]')

plt.ylabel('Degrees ???')
Text(0, 0.5, 'Degrees ???')






