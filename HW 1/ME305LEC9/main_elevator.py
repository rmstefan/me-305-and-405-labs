'''
@file main_elevator.py
Created on Wed Oct 7, 2020
@author: Becca Stefanich
@brief This file containscode for elevator motion
@details The file is available at: https://bitbucket.org/rmstefan/me-305-and-405-labs/src/master/HW%201/ME305LEC9/main_elevator.py
'''


from elevator import Button, MotorDriver, TaskElevator
from lab_1 import fib      

#@image html StateTransitionDiagram.png

## Motor Object
Motor = MotorDriver()

## Button Object for pressing elevator buttons to change floors
button_1 = Button('PB6')
button_2 = Button('PB7')

## Button Object for "left limit"
first = Button('PB8')

## Button Object for "right limit"
second = Button('PB9')

## Task object
task1 = TaskElevator(1, button_1, button_2, first, second, Motor) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to "while True:" once we're on hardware
    task1.run()
    task2 = fib(N)
    task2.run()
