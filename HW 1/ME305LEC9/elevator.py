'''
@file FSM_example.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary windshield
wiper.

@details The file is available at https://bitbucket.org/rmstefan/me-305-and-405-labs/src/master/HW%201/ME305LEC9/elevator.py

The user has a button to turn ON or OFF the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control elevator movement.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
        
    ## Constant defining State 1 
    S1_MOVING_DOWN      = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP        = 2    
    
    ## Constant defining State 3
    S3_STOPPED_AT_FIRST_FLOOR      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_AT_SECOND_FLOOR     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING       = 5
    
    def __init__(self, interval, button_1, button_2, becca, second, Motor): 
        '''
        @brief            Creates a TaskElevator object.
        @param GoButton   An object from class Button representing on/off
        @param LeftLimit  An object from class Button representing the left Limit switch
        @param RightLimit An object from class Button representing the right Limit switch
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used to move to the first sloor
        self.button_1 = button_1
        
        ## The button object used to move to the second floor
        self.button_2 = button_2
        
        ## The button object used for the left limit
        self.first = becca
        
        ## The button object used for the right limit
        self.second = second
        
        ## The motor object "wiping" the wipers
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval  #want to be able to set ourselves
        
        ## The timestamp for the first iteration
        self.start_time = time.time()  # The number of seconds since Jan 1. 1970
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time > self.next_time):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S3_STOPPED_AT_FIRST_FLOOR)
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_AT_FIRST_FLOOR):
                # Run State 1 Code
                
                if(self.runs*self.interval > 10):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if(self.button_2.getButtonState(1)):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Forward()
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                self.transitionTo(self.S4_STOPPED_AT_SECOND_FLOOR)
                self.Motor.Stop()
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Transition to state 1 if the left limit switch is active
                if(self.first.getButtonState(1)):
                    self.transitionTo(self.S3_STOPPED_AT_FIRST_FLOOR)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self, val):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        
        if (val == 1):
            print('stay on floor or go to second floor?')
            self.button = choice([0, 2])
        elif(val == 2):
            print('stay on floor or go to first floor?')
            self.button = choice([0, 1])
            
        #if ButtonValue == 0:
        #    return choice([True, False])
        #else:
        #    return False

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving elevator up')
    
    def Reverse(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving elevator down')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')



























