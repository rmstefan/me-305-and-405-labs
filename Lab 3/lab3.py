# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 18:24:40 2020

@author: Becca Stefanich
"""

import pyb

class encoderoperation: # 2 tasks running at once
    
    def __init__(self):
    # this handles channel info, sets timers, zeros variables
        self.tim = pyb.Timer(4, prescalar=0, period=65535) # do i need to include self. ?
        self.pin1 = pyb.Pin.cpu.B6
        self.pin2 = pyb.Pin.cpu.B7 
        
        #ch1, ch2?

        tim.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        tim.channel(2, pyb.Timer.ENC_AB, pin=pin2)
        
        self.position = 0
        self.prevcount = self.tim.counter() #will probably start near 0
        
        
    def update(self):
    # call update at a regular interval
    # this handles the 'good' or 'bad' delta values
       
        count = self.tim.counter() # gets curent value of count
        delta = count - self.prevcount
        self.prevcount = count
        
        if(delta > 32767):
            self.delta = delta - 65536
        if(delta < -32767):
            self.delta = delta + 65536
                
        self.position = self.position + self.delta
        
    #timer changes when ch1 or ch2 changes
    
    
         
    def get_position(self):
    # this returns the last "update" call - return position value
        return(self.position)
        
    def set_position(self, pos): # return delta value
    # sets the current position to a specified value
        self.position = self.pos
        
    ## get_delta returns the difference in recorded position between the two most recent update() calls
    def get_delta(self): 
        return(self.delta)
        
         
    
    
from pyb import UART

class userinterface:
    # interact with encoder
    
    # must zero encoder position
    
    
    # must print out encoder position
    print(uart.any(position))
    
    # must print out encoder delta
    print(uart.any(delta))
    
    
    
    
    
    
    
    