# -*- coding: utf-8 -*-
"""
@file BlinkLedMain.py
Created on Sun Oct 11, 2020
@author: Becca Stefanich
@brief This file contains code for blinking an LED and simulating another
@details The file is available at https://bitbucket.org/rmstefan/me-305-and-405-labs/src/master/Lab%202/
"""

#set up and import needed items
import pyb
import time

# this is setting up the pin used as an ouput
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## this class creates a virtual LED pattern
class VirtualBlinkLed:
        '''
        @brief runs the blinking of the virtual LED
        '''
        def run(self, interval):
            self.interval = interval  #want to be able to set ourselves
        
            ## The timestamp for the first iteration
            self.start_time = time.time()  # The number of seconds since Jan 1. 1970
            
            while True:
                self.curr_time = time.time()
                if (self.curr_time > self.next_time/2):
                    print('LED OFF')
                elif(self.curr_time > self.next_time):
                    print('LED ON')
                self.next_time = self.start_time + self.interval
                self.runs += 1
    
## this class created actual LED blinking
class BlinkLed:
        '''
        @brief runs the blinking of the LED in a staircase fashion(STEPWISE)
        '''
        def run(self, interval):
            
            self.interval = interval  #want to be able to set ourselves
        
            ## The timestamp for the first iteration
            self.start_time = time.time()  # The number of seconds since Jan 1. 1970
            
            tim2 = pyb.Timer(4, prescaler=0, period=10)
            t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
            while True:
                for idx in range (100):
                    t2ch1.pulse_width_percent(idx)
                    if(idx == 99):
                        idx = 0
                     ## The "timestamp" for when the task should run next
                    self.next_time = self.start_time + self.interval

    