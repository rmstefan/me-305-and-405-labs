# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 19:17:08 2020

@author: Becca Stefanich
"""
import serial
ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)

task1 = encoderoperation()
task2 = userinterface()

taskList = [task1,
            task2]
 
while True:
    for task in taskList:
        task.run()