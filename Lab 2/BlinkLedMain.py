# -*- coding: utf-8 -*-
"""
@file BlinkLedMain.py
Created on Sun Oct 11, 2020
@author: Becca Stefanich
@brief This file contains code for blinking an LED and simulating another
@details The file is available at https://bitbucket.org/rmstefan/me-305-and-405-labs/src/master/Lab%202/
"""

from BlinkLed import VirtualBlinkLed, BlinkLed

## task object
task1 = VirtualBlinkLed(.1)
task2 = BlinkLed(.1)

## run the tasks that we want to run simultaneously
while True:
    task1.run()
    task2.run()