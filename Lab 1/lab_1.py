# -*- coding: utf-8 -*-
'''
Created on Tue Sep 22 08:56:45 2020
@filename lab_1.py
@author: Becca Stefanich
@brief This file contains one way to evaluate the Fibonacci sequence
@details The file is available at https://bitbucket.org/rmstefan/me-305-and-405-labs/src/master/Lab%201/lab_1.py
'''
''' There must be a docstring at the beginning of a Python source file with
 an @file [filename] tag in it! '''
 
 ## to find the fibonacci sequence of a number n, enter as: fib(n)
 ## n must be a positive integer

def fib (idx):
    if idx < 0:
        # the sequence is only valid for POSITIVE integers
        print ('Input must be a positive integer')
    elif idx > 100:
        # to limit computational time, we use a max of 100
        print ('For time saving mode, input integer under 100')
        ''' this is for the purpose of this lab, we only need to model numbers 
        up to 100 to show that this works or else computation time can get too 
        large '''
    else:
        list = [0]*(idx + 1);
        ''' this creates an empty list with zeros as placeholders so we can 
        add our fibonacci sequence in this'''
        if idx == 0:
            list[0] = 0;
            # special case for n = 0
        
        elif idx == 1:
            list[0] = 0;
            list[1] = 1;
            # special case for n = 1
            
        else:
            list[0] = 0;
            list[1] = 1;
            place = 2;
            ''' "place" is a placeholder for idx so that this while loop
            does not keep repeating for eternity'''
            while place <= idx:
                ''' the fib number at any given index is the sum of the 
                value before the given index and the value two steps behind
                the given index'''
                list[place] = list[place - 2] + list[place - 1]
                # this is the fibonacci sequence definition in action
                place = place + 1;
                # place is the placeholder for which term in the sequence we are calculating
            
        print ('Calculating Fibonacci number at  '
           'index n = {:}.'.format(idx))
        return list
    ''' returning the list give us the fibonacci sequence'''
    
    
''' def fib defined the fibonacci function, it is going to calculate a 
Fibonacci number sequence at a certain index'''


if __name__ == '__main__':
    ''' setting name = main. main is another python document?
    code here will only run when the script is executed as a standalone 
    program (hit green arrow), used to test code. can print what you would 
    call out in console in this section'''
    fib(0)    